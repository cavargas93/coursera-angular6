var express = require('express'), cors = require('cors');
//el cors lo usamos Para que el api admita solicitudes que vienen de páginas de otros dominios/puertos.
var app = express();
app.use(express.json());
app.use(cors());
app.listen(3000, () => console.log("Server running on port 3000"));

var ciudades = ["Paris", "Bacelona", "Barranquilla", "Cali", "Montevideo", "Mexico DF", "Nueva York"];
app.get("/ciudades", (req, res, next) => res.json(ciudades.filter((c) => c.toLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1)));

var misDestinos = [];
app.get("/my", (req, res, next) => res.json(misDestinos));
app.post("/my", (req, res, next) => {
    console.log(req.body);
    misDestinos.push(req.body.nuevo)
    res.json(misDestinos);
})

// Web service de traducciones
app.get("/api/translation", (req, res, next) => res.json([
    {lang: req.query.lang, key: 'HOLA', value: 'HOLA ' + req.query.lang}
]));