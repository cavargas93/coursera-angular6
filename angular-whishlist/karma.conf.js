// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-coverage'),
      require('@angular-devkit/build-angular/plugins/karma')
    ],
    client: {
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    coverageReporter: {
      dir: require('path').join(__dirname, './coverage/angular-whishlist'),
      subdir: '.',
      reporters: [
        { type: 'html' },
        { type: 'text-summary' }
      ]
    },
    reporters: ['progress', 'kjhtml'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: false,
    browsers: ['Chrome', 'ChromeHeadless', 'ChromeHeadlessCI'],
    /* Esto es algo interno de Karma, básicamente lo que le estamos diciendo es que los test los tiene que correr adentro de un navegador, ese navegador, vamos a usar un Chrome Headless, es decir, algo que no, va a tener un Chrome instalado, este servidor, adentro de la nube, cloud de Circle CI, va a haber un servidor que se va a bajar nuestro código, va a correr nuestros tests e internamente para correr los tests va a usar un Google Chrome, un navegador Google Chrome, pero queremos que lo corra en modo Headless. En modo Headless significa que no abre, es un modo no interactivo, no abre la ventana gráfica, esto es para garantizar que no tire error en ejecución si el servidor de Circle CI no tiene instaladas las librerías de gráficos del sistema operativo donde esto esté corriendo, básicamente esa es la justificación de por qué hay que hacerlo, y you viene un Chrome Headless por defecto configurado pero de acuerdo a la documentación actual, al de la fecha noviembre 2018, recomiendan sobreescribir, agregar algunas configuraciones, por ende te recomiendan en la actualidad configurar un Chrome Headless CI, que es algo Custom, como ven por eso estamos dentro de Custom Launchers. Que herede todas las propiedades de un Chrome Headless, como viene definido por defecto en karma.
Reproduce el video desde 

Y a su vez que se le customicen, se le personalicen varios de estos flats. */
    customLaunchers: {
      ChromeHeadlessCI: {
        base: 'ChromeHeadless',
        flags: ['--no-sandbox', '--disable-gpu', '--disable-translate', '--disable-extensions', '--remote-debugging-port=9223']
      }
    },
    singleRun: false,
    restartOnFileChange: true
  });
};
