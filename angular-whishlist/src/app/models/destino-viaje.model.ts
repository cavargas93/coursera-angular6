export class DestinoViaje {
	private selected: boolean;
	public servicios: string[];
	id: string;
	constructor(public nombre:string,public u:string, public votes:number = 0) { 
		this.servicios = ['piscina', 'desayuno', 'Tours']
	}
	isSelected(): boolean {
		return this.selected;
	}
	setSelected(s: boolean) {
		this.selected = s;
	}
	voteUp() {
		this.votes++;
	}
	voteDown() {
		this.votes--;
	}
}
