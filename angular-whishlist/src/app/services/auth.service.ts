import { Injectable } from '@angular/core';

// servicio inyectable
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }
  login(user: string, password: string): boolean {
    if (user === 'user' && password === 'password') {
      //objeto html5 para guardar componentes en el navegador, persiste un valor username en el navegador
      localStorage.setItem('username', user);
      return true;
    }
    return false;
  }

  logout(): any {
    localStorage.removeItem('username');
  }

  getUser(): any {
    return localStorage.getItem('username');
  }

  isLoggedIn(): boolean {
    return this.getUser() !== null;
  }
}